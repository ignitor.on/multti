<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\ApiControllerTrait;

class UserController extends Controller
{
  use ApiControllerTrait {}

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    try {
      $query = User::query();
      $users = $query->orderBy('id', 'Desc')->paginate(20);

      return $this->createResponse([
        "users"   => $users
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $rulesInputsUsers = $this->getRulesInputsUsers();

      $validateUsers = $this->validateInputs($request, $rulesInputsUsers);
      $responseValidateUsers =  $validateUsers->original;

      if (isset($responseValidateUsers->error)) {
        return $validateUsers;
      }

      $user = [
        "name"     => $request->name,
        "email"    => $request->email,
        "phone"    => $request->phone,
        "password" => Hash::make($request->password)
      ];

      $user = User::create($user);

      return $this->createResponse([
        "user" => $user
      ], 201);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    try {
      $user = User::find($id);

      return $this->createResponse([
        "user"   => $user
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      $rulesInputsUsers = $this->getRulesInputsUsers();

      $validateUsers = $this->validateInputs($request, $rulesInputsUsers);
      $responseValidateUsers =  $validateUsers->original;

      if (isset($responseValidateUsers->error)) {
        return $validateUsers;
      }

      $user = User::find($id);

      if (!$user) {
        return $this->createResponse([
          'message' => 'Usuário não encontrado.',
        ], 404);
      }

      $user->name     = $request->name;
      $user->email    = $request->email;
      $user->phone    = $request->phone;
      $user->password = Hash::make($request->password);

      $user->update();

      return $this->createResponse([
        "user" => $user
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      $user = User::find($id);

      if (!$user) {
        return $this->createResponse([
          'message' => 'Usuário não encontrado.',
        ], 422);
      }

      $user->delete();

      return $this->createResponse([
        "message"  => 'Usuário deletado com sucesso.',
      ], 200);
    } catch (\Throwable $th) {
      return $this->createResponse([
        "message" => $th->getMessage(),
        "error"   => true
      ], 500);
    }
  }

  public function getRulesInputsUsers()
  {
    return [
      'name'     => 'required',
      'email'    => 'required',
      'phone'    => 'required',
      'password' => 'required'
    ];
  }

}
